// import {GoogleMapsAPIWrapper} from 'angular2-google-maps/core/services/google-maps-api-wrapper';
import { GoogleMapsAPIWrapper } from "@agm/core";
import { Directive, Input } from '@angular/core';

declare var google: any;


@Directive({
  selector: 'agm-directions'
})
export class DirectionsMapDirective {
  @Input() origin;
  @Input() destination;
  @Input() waypoints;

  constructor(private gmapsApi: GoogleMapsAPIWrapper) {
  }

  ngOnInit() {
    this.gmapsApi.getNativeMap().then(map => {
      var directionsService = new google.maps.DirectionsService;

      directionsService.route({
        origin: {lat: this.origin.latitude, lng: this.origin.longitude},
        destination: {lat: this.destination.lat, lng: this.destination.lng},
        waypoints: [],
        optimizeWaypoints: true,
        travelMode: 'DRIVING',
        provideRouteAlternatives: true,
      }, function (response, status) {
        if (status === 'OK') {
          new google.maps.DirectionsRenderer({
            map: map,
            directions: response,
            routeIndex: 0
          });
        } else {
          window.alert('Directions request failed due to ' + status);
        }
      });

    });
  }
}
