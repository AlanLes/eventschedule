export class EventDataModel {
  constructor(public event?: EventModel,
              public location?: LocationModel) {
    event ? this.event = event : this.event = new EventModel();
    location ? this.location = location : this.location = new LocationModel();
  }
}

export class DateTimeModel {
  constructor(public dateTimeEnd: string = '',
              public dateTimeStart: string = '') {
  }
}

export class EventModel {
  constructor(public dateTime?: DateTimeModel,
              public details?: DetailsModel,
              public id: string = '',
              public name: string = '') {
    dateTime ? this.dateTime = dateTime : this.dateTime = new DateTimeModel();
    details ? this.details = details : this.details = new DetailsModel();
  }
}

export class LocationModel {
  constructor(public address: string = '',
              public latitude: number = 0,
              public longitude: number = 0,
              public name: string = '') {
  }
}

export class DetailsModel {
  constructor(public city: string = '',
              public eventSize: string = '',
              public sportType: string = '') {
  }
}

export class FilterModel {
  constructor(public description: string,
              public name: string,
              public values: string[]){
  }
}

export class PreparedFilterModel {
  constructor(public description: string,
              public name: string,
              public values: FilterValue[]){
  }
}

export class FilterValue {
  constructor(public name: string,
              public checked: boolean){
  }
}

//////////////////////////////////////////////prepared

export class EventsByDay {
  constructor(public dateDay: string,
              public events: EventDataModel[]){
  }
}

export class TripModel {
  constructor(public tripName: string,
              public eventsToTrip: EventDataModel[],
              public id: string) {
  }
}

export class GoogleWaypointModel {
  constructor(public location: GoogleLocationModel,
              public stopover: boolean = true){
  }
}

export class GoogleLocationModel {
  constructor(public lat: number,
              public lng: number,
              public description?: string) {
  }
}
