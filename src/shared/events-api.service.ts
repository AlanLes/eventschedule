import { Injectable } from '@angular/core';
import { Http, Response } from "@angular/http";
import 'rxjs';
import { Observable } from 'rxjs/Observable';
import { EventDataModel, FilterModel } from "./events-data.models";
import * as _ from 'lodash';

@Injectable()
export class EventsApiService {

  private baseUrl = 'https://events-schedule-i2.firebaseio.com/';
  private currentEventData: EventDataModel = {};
  private eventData: EventDataModel = {};

  constructor(private http: Http) {
  }

  public getAllEvents(): Promise<EventDataModel[]> {
    let tmpArr = [];
    return new Promise<EventDataModel[]>((resolve, reject) => {
      this.http.get(`${this.baseUrl}/events-data.json`)
        .subscribe(
          res => {
            _.forEach(res.json(), (val, key) => {
              tmpArr.push(val);
            });
            return resolve(tmpArr);
          },
          err => reject(err.json())
        );
    })
  }

  public getAvailableFilters(): Promise<FilterModel[]> {
    return new Promise<FilterModel[]>((resolve, reject) => {
      this.http.get(`${this.baseUrl}/availableFilters.json`)
        .subscribe(
          res => resolve(res.json()),
          err => reject(err.json())
        );
    })
  }

  public getEventById(id: string, forceRefresh: boolean = false): Observable<EventDataModel> {
    if (!forceRefresh && this.eventData[id]) {
      this.currentEventData = this.eventData[id];
      return Observable.of(this.currentEventData);
    } else {
      return this.http.get(`${this.baseUrl}/events-data/${id}.json`)
        .map((response: Response) => {
          this.eventData[id] = this.currentEventData = response.json();
          return this.currentEventData;
        });
    }
  }

  public getCurrentEvent(): EventDataModel{
    return this.currentEventData;
  }


  public refreshEvent(): Observable<EventDataModel> {
    return this.getEventById(this.currentEventData.event.id, true);
  }


}
