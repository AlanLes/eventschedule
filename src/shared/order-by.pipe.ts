import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';

@Pipe({
  name: 'orderBy',
})
export class OrderByPipe implements PipeTransform {
  transform(array, args?, order?) {
    if (args) {
      if (order === 'asc') {
        return _.sortBy(array, args);
      } else if (order === 'desc') {
        return _.sortBy(array, args).reverse();
      }
    } else {
      return array;
    }

  }
}
