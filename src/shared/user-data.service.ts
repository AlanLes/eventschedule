import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';
import { Storage } from "@ionic/storage";
import { Geolocation } from '@ionic-native/geolocation'
import { TripModel, EventDataModel } from "./events-data.models";
import { GuidGenerator } from "./utilities";


@Injectable()
export class UserDataService {

  currentPosition;

  constructor(private events: Events, private storage: Storage, private geolocation: Geolocation) {
  }

  public addEventToFollowed(event: EventDataModel): Promise<void> {
    let tmp = event;
    return new Promise<void>(resolve => {
      this.storage.set(tmp.event.id, JSON.stringify(tmp)).then(() => {
        this.events.publish('followedEvents:refresh');
        resolve();
      });
    })
  }

  public removeEventFromFollowed(id: string): Promise<void> {
    return new Promise<void>(resolve => {
      this.storage.remove(id.toString()).then(() => {
        this.events.publish('followedEvents:refresh');
        resolve();
      })
    })
  }

  public getAllFollowedEvents(): Promise<EventDataModel[]> {
    let items = [];
    return new Promise<any[]>(resolve => {
      this.storage.forEach((v, k, i) => {
        //check if it's an event
        if (JSON.parse(v)['event']) {
          items.push(JSON.parse(v));
        }
      }).then(() => resolve(items))
    })
  }

  public isInDatabase(id: string): Promise<boolean> {
    return new Promise<boolean>(resolve => {
      resolve(this.storage.get(id.toString()).then(value => !!value));
    })
  }

  public getPosition(): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      if (!this.currentPosition) {
        this.geolocation.getCurrentPosition()
          .then(res => {
            this.currentPosition = res;
            resolve(res)
          })
          .catch(err => reject(err));
      } else {
        resolve(this.currentPosition);
      }
    })
  }

  public saveTripToDB(tripName: string, eventsToTrip: EventDataModel[]): Promise<TripModel> {
    return new Promise<TripModel>((resolve, reject) => {
      let dbTripRecord: TripModel = {
        tripName: tripName,
        eventsToTrip: eventsToTrip,
        id: GuidGenerator.newGuid()
      };
      this.storage.set(dbTripRecord.id, JSON.stringify(dbTripRecord))
        .then(() => {
          this.events.publish('savedTrips:refresh');
          resolve(dbTripRecord);
        })
        .catch(() => reject());
    })
  }

  public getAllSavedTrips(): Promise<TripModel[]> {
    let items: TripModel[] = [];
    return new Promise<TripModel[]>((resolve) => {
      this.storage.forEach((v, k, i) => {
        //check if it's a tripRecord
        if (JSON.parse(v)['tripName']) {
          items.push(JSON.parse(v));
        }
      }).then(() => resolve(items))
    })
  }

  public removeTripFromDB(id: string): Promise<void> {
    return new Promise<void>(resolve => {
      this.storage.remove(id.toString()).then(() => {
        this.events.publish('savedTrips:refresh');
        resolve();
      })
    })
  }

}
