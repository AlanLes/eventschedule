import { EventsByDay, EventDataModel } from "./events-data.models";
import * as moment from "moment";
import * as _ from 'lodash';

export function getDistanceFromLatLngInKm(lat1,lng1,lat2,lng2) {
  let R = 6371; // Radius of the earth in km
  let dLat = deg2rad(lat2-lat1);  // deg2rad below
  let dLon = deg2rad(lng2-lng1);

  let a =
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
    Math.sin(dLon/2) * Math.sin(dLon/2);

  let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

  let distance = R * c; // Distance in km
  return distance;
}

export function deg2rad(deg) {
  return deg * (Math.PI/180)
}


// taken from https://stackoverflow.com/a/26502275/5625536
export class GuidGenerator {
  static newGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
  }
}

export function prepareEventsByDays(events: EventDataModel[]): EventsByDay[] {
  events.map(el => {
    el['dateDay'] = moment(el.event.dateTime.dateTimeStart).format("DD.MM.YYYY");
    return el;
  });
  return _.chain(events)
    .groupBy('dateDay')
    .toPairs()
    .map(el => _.zipObject(['dateDay', 'events'], el))
    .value();
}
// Example of a bunch of GUIDs
// for (var i = 0; i < 100; i++) {
//   var id = Guid.newGuid();
//   console.log(id);
// }
