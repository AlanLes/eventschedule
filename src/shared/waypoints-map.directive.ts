// import {GoogleMapsAPIWrapper} from 'angular2-google-maps/core/services/google-maps-api-wrapper';
import { GoogleMapsAPIWrapper } from "@agm/core";
import { Directive, Input } from '@angular/core';
import { GoogleWaypointModel } from "./events-data.models";

declare var google: any;


@Directive({
  selector: 'agm-waypoints'
})
export class WaypointsMapDirective {
  @Input() origin;
  @Input() destination;
  @Input() waypoints: GoogleWaypointModel[];
  private wpts: GoogleWaypointModel[];

  constructor(private gmapsApi: GoogleMapsAPIWrapper) { }

  // https://developers.google.com/maps/documentation/javascript/directions#TransitOptions
  // https://developers.google.com/maps/documentation/javascript/directions#Waypoints


  // https://developers.google.com/maps/documentation/javascript/directions#UsingWaypoints
  // KLUCZ /\


  ngOnInit() {
    this.wpts = JSON.parse(JSON.stringify(this.waypoints));

    this.gmapsApi.getNativeMap().then(map => {
      var directionsService = new google.maps.DirectionsService;
      let destination = this.wpts[(this.wpts.length - 1)].location;
      this.wpts.pop(); //remove last element from waypoints array

      directionsService.route({
        origin: this.origin,
        destination: destination,
        travelMode: 'DRIVING',
        optimizeWaypoints: false,
        waypoints: this.wpts,
        provideRouteAlternatives: true,
      }, (response, status) => {
        if (status === 'OK') {
          console.log('response', response);

          let len = response.routes[0].legs.length;

          for (let i = 1; i < len; i++) {
            response.routes[0].legs[i].start_address = this.wpts[(this.wpts.length - 1)].location.description;
            response.routes[0].legs[i-1].end_address = this.wpts[(this.wpts.length - 1)].location.description;
          }
          response.routes[0].legs[len-1].end_address = destination.description;

          let directionsDisplay = new google.maps.DirectionsRenderer({
            map: map,
            directions: response
          });

          directionsDisplay.setPanel(document.getElementById('info-panel'));

        } else {
          window.alert('Directions request failed due to ' + status);
        }
      });

    });
  }
}


// jak sobie klikniesz w marker to chcesz wiedzieć coś o evencie/
