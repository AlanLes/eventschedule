import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { AgmCoreModule, GoogleMapsAPIWrapper } from '@agm/core';
import { StatusBar } from '@ionic-native/status-bar';
import { Geolocation } from "@ionic-native/geolocation"
import { SplashScreen } from '@ionic-native/splash-screen';
import { IonicStorageModule } from '@ionic/storage';
//pages
import { MyApp } from './app.component';
import { MyEventsPage } from "../pages/my-events/my-events.page";
import { EventsListPage } from "../pages/events-list/events-list.page";
import { EventDetailsPage } from "../pages/event-details/event-details.page";
import { MapPage } from "../pages/map/map.page";
import { SettingsPopoverPage } from "../pages/settings-popover/settings-popover.page";
//shared
import { EventsApiService } from "../shared/events-api.service";
import { UserDataService } from "../shared/user-data.service";
import { DirectionsMapDirective } from "../shared/directions-map.directive";
import { OrderByPipe } from "../shared/order-by.pipe";
import { TripPage } from "../pages/trip/trip.page";
import { TripMapPage } from "../pages/trip-map/trip-map.page";
import { TripListPage } from "../pages/trip-list/trip-list.page";
import { WaypointsMapDirective } from "../shared/waypoints-map.directive";


@NgModule({
  declarations: [
    MyApp,
    MyEventsPage,
    EventsListPage,
    TripPage,
    EventDetailsPage,
    MapPage,
    TripMapPage,
    TripListPage,
    SettingsPopoverPage,
    DirectionsMapDirective,
    WaypointsMapDirective,
    OrderByPipe
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCtcnunkT-WypcW8LTAi36A5zz9VyVYMn8'
    }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    MyEventsPage,
    EventsListPage,
    TripPage,
    EventDetailsPage,
    MapPage,
    TripMapPage,
    TripListPage,
    SettingsPopoverPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    EventsApiService,
    UserDataService,
    GoogleMapsAPIWrapper
  ]
})
export class AppModule {
}
