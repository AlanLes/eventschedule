import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';

import { MyEventsPage } from "../pages/my-events/my-events.page";
import { EventsListPage } from "../pages/events-list/events-list.page";
import { UserDataService } from "../shared/user-data.service";
import { TripListPage } from "../pages/trip-list/trip-list.page";
import { EventDataModel } from "../shared/events-data.models";


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = MyEventsPage;
  pages: any[];

  followedEvents: EventDataModel[];

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,
              public storage: Storage, public events: Events, public userDataService: UserDataService) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Lista wydarzeń', component: EventsListPage },
      { title: 'Zapisane podróże', component: TripListPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.refreshFollowedEvents();
      this.events.subscribe('followedEvents:refresh', () => this.refreshFollowedEvents())
    });
  }

  public openPage(page) {
    this.nav.push(page.component);
  }

  private refreshFollowedEvents() {
    this.userDataService.getAllFollowedEvents().then(res => this.followedEvents = res);
  }
}
