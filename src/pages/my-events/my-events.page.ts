import { Component, ViewChild } from '@angular/core';
import { List, Content } from 'ionic-angular';
import { AlertController, NavController, ToastController } from 'ionic-angular';
import { EventsListPage } from "../events-list/events-list.page";
import { EventDetailsPage } from "../event-details/event-details.page";
import { TripPage } from "../trip/trip.page";
import { UserDataService } from "../../shared/user-data.service";
import { EventDataModel } from "../../shared/events-data.models";
import * as _ from 'lodash';
import * as moment from "moment";

declare var google: any;

@Component({
  selector: 'my-events',
  templateUrl: 'my-events.page.html',
})
export class MyEventsPage {
  @ViewChild(List) list: List;
  @ViewChild(Content) content: Content;

  originFollowedEvents: EventDataModel[];
  followedEvents: EventDataModel[];

  sortByDate: string = '';
  sortByCity: string = '';
  orderPhrase: string = '';

  allowCheckEvents: boolean;
  allEventsChecked: boolean;
  eventsToTrip: EventDataModel[];

  private eventsAreOverlapsing: boolean = true;

  constructor(private nav: NavController,
              private alertController: AlertController,
              private toastController: ToastController,
              private userDataService: UserDataService) {
    this.followedEvents = [];
    this.eventsToTrip = [];
  }

  ionViewWillEnter() {
    this.userDataService.getAllFollowedEvents().then(res => {
      this.followedEvents = _.cloneDeep(res);
      this.originFollowedEvents = _.cloneDeep(res);
    });
  }

  ionViewDidLeave() {
    if (this.allowCheckEvents) {
      this.allowCheckEvents = false;
    }
  }

  public goToEventsList(): void {
    this.nav.push(EventsListPage);
  }

  public selectEvent($event, id: string): void {
    this.nav.push(EventDetailsPage, id);
  }

  public sortBy(prop: string): void {
    switch (prop) {
      case 'date': {
        this.sortByDate === 'asc' ? this.sortByDate = 'desc' : this.sortByDate = 'asc';
        this.sortByCity = '';
        this.orderPhrase = 'event.dateTime.dateTimeStart';
        break;
      }
      case 'city': {
        this.sortByCity === 'asc' ? this.sortByCity = 'desc' : this.sortByCity = 'asc';
        this.sortByDate = '';
        this.orderPhrase = 'event.details.city';
        break;
      }
    }
  }

  public unfollowEvent(event: EventDataModel): void {
    let confirm = this.alertController.create({
      title: 'Usunąć z obserwowanych?',
      message: 'Czy na pewno chcesz usunąć ten element z listy obserwowanych wydarzeń?',
      buttons: [
        {
          text: 'Tak',
          handler: () => {
            this.userDataService.removeEventFromFollowed(event.event.id).then(() => {
              _.remove(this.originFollowedEvents, (el) => el.event.id === event.event.id);
              this.followedEvents = _.cloneDeep(this.originFollowedEvents);

              let toast = this.toastController.create({
                duration: 1500,
                message: 'Z powodzeniem usunięto element z listy obserwowanych wydarzeń.',
                position: 'bottom'
              });
              toast.present();
            });
          }
        },
        {
          text: 'Nie',
          handler: () => {
            this.list.closeSlidingItems();
          }
        }
      ]
    });
    confirm.present();
  }

  public planYourTrip(): void {
    let toast = this.toastController.create({
      duration: 2500,
      message: 'Aby zaplanować podróż zaznacz wydarzenia, w których chcesz uczestniczyć.',
      position: 'bottom'
    });
    toast.present().then(() => {
      this.allowCheckEvents = true
    })
  }

  public cancelTripPlanning(): void {
    this.allowCheckEvents = false;
    this.eventsToTrip = []; //cancelTripPlanning so eventsToTrip array can be empty again;
    this.followedEvents = _.cloneDeep(this.originFollowedEvents); //on cancelTripPlanning followedEvents can be again as origin;
  }

  public checkAllEvents() {
    if (this.allEventsChecked) {
      this.followedEvents.map(el => {
        el['toTrip'] = true;
        return el;
      });
    }
    else {
      this.followedEvents.map(el => {
        el['toTrip'] = false;
        return el;
      });
    }
  }

  public uncheckAllEvents($event) {
    if (!$event.checked) {
      this.allEventsChecked = false;
    }
  }

  public validateEvents(): void {
    this.clearOverlapsEvents();                                                                 //1.
    this.addEventsToTrip();                                                                     //2.

    if (this.eventsToTrip.length) {

      this.eventsToTrip = this.sortEventsToTripByDate(this.eventsToTrip);                       //3.

      this.checkOverlapsing(this.eventsToTrip)
        .then(overlapsEvents => {                                                     //4. 5. 6.
          this.followedEvents = _.unionBy(overlapsEvents, this.followedEvents, 'event.id');

          if (!this.eventsAreOverlapsing) {
            this.nav.push(TripPage, {eventsToTrip: this.eventsToTrip});                 // 7.
          }
        });
    } else {
      let alert = this.alertController.create({
        title: 'Planowanie podróży',
        subTitle: 'Aby przejść dalej musisz zaznaczyć przynajmniej jedno wydarzenie, w którym chcesz uczestniczyć.',
        buttons: ['OK']
      });
      alert.present();
    }
  }

  // 1. set .overlaps property to false in all the followedEvents - avoid them to be red highlightened before belows validate process starts
  private clearOverlapsEvents(): void {
    this.followedEvents.map(el => {
      el['overlaps'] = false;
      return el;
    });
  }

  // 2. prepare eventsToTrip[] array - filter followedEvents and
  // add these which have .toTrip==true to the mentioned eventsToTrip[] array
  private addEventsToTrip(): void {
    this.eventsToTrip = this.followedEvents.filter(el => el['toTrip']);
  }

  // 4. check if user is able to visit selected events -
  // if these events do not overlaps
  // or if trip's duration between each two cities isn't too big.
  private checkOverlapsing(events: EventDataModel[]): Promise<EventDataModel[]> {
    return new Promise<EventDataModel[]>(resolve => {
      let overlapsEvents = this.checkOverlapsingTimes(events);
      if (overlapsEvents.length) {
        resolve(overlapsEvents);
      } else {
        this.checkOverlapsingTravels(events)
        .then(events => {
          if (events.length) {
            let toast = this.toastController.create({
              duration: 3000,
              message: 'Podróż pomiędzy niektórymi wydarzeniami będzie trwała zbyt długo, aby się przemieścić. Musisz z czegoś zrezygnować.',
              position: 'bottom'
            });
            toast.present().then(() => {
              this.content.scrollToTop();
              this.eventsAreOverlapsing = true;
              resolve(events);
            });
          } else {
            this.eventsAreOverlapsing = false;
            resolve(events);
          }
        });
      }
    })
  }

  // 5. checkOverlapsingTimes() - if these events are overlapsing then change their .overlaps property on true;
  private checkOverlapsingTimes(events: EventDataModel[]): EventDataModel[] {
    let overlapsEvents = [];
    _.forEach(events, (ev1, k1) => {
      _.forEach(events, (ev2, k2) => {
        if (ev1.event.id !== ev2.event.id) {
          //    DateRangesOverlap = max(start1, start2) < min(end1, end2)
          //    https://stackoverflow.com/questions/325933/determine-whether-two-date-ranges-overlap
          if (moment.max(moment(ev1.event.dateTime.dateTimeStart), moment(ev2.event.dateTime.dateTimeStart)) <
            moment.min(moment(ev1.event.dateTime.dateTimeEnd), moment(ev2.event.dateTime.dateTimeEnd))) {
            ev1['overlaps'] = true;
            ev2['overlaps'] = true;
            overlapsEvents.push(ev1, ev2);
          }
        }
      })
    });

    overlapsEvents = _.intersection(overlapsEvents);
    if (overlapsEvents.length) {
      let toast = this.toastController.create({
        duration: 3000,
        message: 'Niektóre wydarzenia trwają w tym samym czasie. Nie dasz rady być w dwóch miejscach jednocześnie.',
        position: 'bottom'
      });
      toast.present().then(() => {
        this.content.scrollToTop();
        this.eventsAreOverlapsing = true;
      });
    } else {
      this.eventsAreOverlapsing = false;
    }
    return overlapsEvents;
  }

  // 6. checkOverlapsingTravels() - if traveling time between two following events is longer than
  // duration between the dateTimeEnd of first and dateTimeStart last
  // then change their .overlaps property on true and notify the user
  // private checkOverlapsingTravels(events: EventDataModel[]): Promise<EventDataModel[]> {
  //   return new Promise<EventDataModel[]>(resolve => {
  //     let directionsService = new google.maps.DirectionsService;
  //
  //     let overlapsEvents = [];
  //
  //     for (let i = 0; i < (events.length - 1); i++) {
  //       let origin = {lat: events[i].location.latitude, lng: events[i].location.longitude};
  //       let dest = {lat: events[i + 1].location.latitude, lng: events[i + 1].location.longitude};
  //       directionsService.route({
  //         origin: origin,
  //         destination: dest,
  //         optimizeWaypoints: true,
  //         travelMode: 'DRIVING',
  //         provideRouteAlternatives: true,
  //       }, (response) => {
  //         let allowedTimeGap =
  //           moment(events[i + 1].event.dateTime.dateTimeStart, 'YYYY-MM-DD HH:mm')
  //             .diff(moment(events[i].event.dateTime.dateTimeEnd, 'YYYY-MM-DD HH:mm'), 'seconds');
  //
  //         // check if the duration.value [secs] is bigger than allowed time gap
  //         if (allowedTimeGap < response.routes[0].legs[0].duration.value) {
  //           events[i]['overlaps'] = true;
  //           events[i + 1]['overlaps'] = true;
  //           overlapsEvents.push(events[i], events[i + 1]);
  //         }
  //         //it's the end of the loops; quite primitive..
  //         if (i === (events.length - 2)) {
  //           overlapsEvents = _.intersection(overlapsEvents);
  //           resolve(overlapsEvents);
  //         }
  //       });
  //     }
  //   })
  // }


  // 6. checkOverlapsingTravels() - if traveling time between two following events is longer than
  // duration between the dateTimeEnd of first and dateTimeStart last
  // then change their .overlaps property on true and notify the user
  private checkOverlapsingTravels(events: EventDataModel[]): Promise<EventDataModel[]> {
    return new Promise(resolve => {
      this.googlePromises(events).then(res => {
        let result = res.sort(function (a, b) {return b.length - a.length;})[0];
        resolve(result);
      })
    });
  }

  private googlePromises(events: EventDataModel[]) {
    let promisesArr: Array<any> = [];
    let directionsService = new google.maps.DirectionsService;
    let overlapsEvents = [];

    for (let i = 0; i < (events.length - 1); i++) {
      let origin = {lat: events[i].location.latitude, lng: events[i].location.longitude};
      let dest = {lat: events[i + 1].location.latitude, lng: events[i + 1].location.longitude};

      promisesArr.push(new Promise(resolve => {
        directionsService.route({
          origin: origin,
          destination: dest,
          optimizeWaypoints: true,
          travelMode: 'DRIVING',
          provideRouteAlternatives: true,
        }, (response) => {
          let allowedTimeGap =
            moment(events[i + 1].event.dateTime.dateTimeStart, 'YYYY-MM-DD HH:mm')
              .diff(moment(events[i].event.dateTime.dateTimeEnd, 'YYYY-MM-DD HH:mm'), 'seconds');

          // check if the duration.value [secs] is bigger than allowed time gap
          if (allowedTimeGap < response.routes[0].legs[0].duration.value) {
            events[i]['overlaps'] = true;
            events[i + 1]['overlaps'] = true;
            overlapsEvents.push(events[i], events[i + 1]);
          }
          //it's the end of the loops; quite primitive..
          overlapsEvents = _.intersection(overlapsEvents);

          resolve(overlapsEvents);
        });
      }));
    }

    return Promise.all(promisesArr);
  }


  // 3. sortEventsToTripByDate() - earliest first
  private sortEventsToTripByDate(eventsToTrip: EventDataModel[]): EventDataModel[] {
    let sortedEvents = _.sortBy(eventsToTrip, ['event.dateTime.dateTimeStart']);
    return sortedEvents;
  }
}
