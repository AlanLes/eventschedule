import { Component } from '@angular/core';
import { AlertController, LoadingController, NavParams } from 'ionic-angular';
import { EventsByDay, GoogleWaypointModel, GoogleLocationModel } from "../../shared/events-data.models";
import { UserDataService } from "../../shared/user-data.service";
import * as moment from "moment";

@Component({
  selector: 'trip-map',
  templateUrl: 'trip-map.page.html',
})
export class TripMapPage {

  public eventsByDay: EventsByDay;
  public waypoints: GoogleWaypointModel[] = [];
  public waypoints2: GoogleWaypointModel[] = [];
  public map: any = {};
  public currentPosition: GoogleLocationModel;
  public destination: GoogleLocationModel;
  dataDownloaded: boolean = false;
  public showMap: boolean = true;

  constructor(private navParams: NavParams,
              private loadingController: LoadingController,
              private alertController: AlertController,
              private userDataService: UserDataService) {
  }

  ionViewWillLoad() {
    this.eventsByDay = this.navParams.data;
    this.prepareWaypointsArray();

    let loader = this.loadingController.create({
      content: 'Pobieram informacje o Twoim aktualnym położeniu...',
      spinner: 'bubbles',
      // dismissOnPageChange: true    //it's broken in the present ionic version
    });
    loader.present().then(() => {
      this.userDataService.getPosition()
        .then(res => {
          console.log(res);
          this.currentPosition = new GoogleLocationModel(res.coords.latitude, res.coords.longitude);
          this.map = {
            lat: res.coords.latitude,
            lng: res.coords.longitude,
            markerLabel: 'dupa',
            zoom: 14
          };
          loader.dismiss();
          this.dataDownloaded = true;
        })
        .catch(err => {
          loader.dismiss();
          let alert = this.alertController.create({
            title: 'Błąd',
            subTitle: 'Wystąpił błąd podczas pobierania informacji o Twoim aktualnym położeniu. Spróbuj ponownie później.',
            buttons: ['OK']
          });
          alert.present();
        });
    })
  }

  private prepareWaypointsArray(): void {
    this.eventsByDay.events.forEach(event => {
      console.log(event);
      let evName = `${event.event.name}`;
      let evStart = `${moment(event.event.dateTime.dateTimeStart, 'YYYY-MM-DD HH:mm').format('HH:mm')}`;
      let evLoc = `${event.location.name}`;
      let desc = `${evName} - ${evLoc}, ${evStart}`;
      this.waypoints.push(new GoogleWaypointModel(new GoogleLocationModel(event.location.latitude, event.location.longitude, desc)));
      this.waypoints2 = JSON.parse(JSON.stringify(this.waypoints));
    });
    console.log(this.waypoints);
  }

}
