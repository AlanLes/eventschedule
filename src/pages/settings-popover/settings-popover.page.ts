import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { PreparedFilterModel } from "../../shared/events-data.models";
import * as moment from "moment";

@Component({
  templateUrl: 'settings-popover.page.html',
  selector: 'settings-popover'
})
export class SettingsPopoverPage {

  filterEnabled: boolean;
  filters: PreparedFilterModel[];

  now: string;
  dateFilterFrom: string;
  dateFilterTo: string;

  constructor(private navParams: NavParams, private viewController: ViewController) {
    this.now = moment().format("YYYY-MM-DD");

    if (this.navParams.data) {
      this.filterEnabled = this.navParams.data.filterEnabled;
      this.filters = this.navParams.data.filters;
      this.dateFilterFrom = this.navParams.data.dateFilterFrom;
      this.dateFilterTo = this.navParams.data.dateFilterTo;
    }
  }

  public toggleFilter(): void {
    console.log("toggle");
  }

  public changeDate(): void {
    //if the selected dateFilterFrom is later then dateFilterTo ->
    //prevent to set a negative time period like from 03.07.2017 to 01.07.2017
    if (moment(this.dateFilterFrom).isAfter(moment(this.dateFilterTo))) {
      this.dateFilterTo = this.dateFilterFrom;
    }
  }

  public close(): void {
    this.viewController.dismiss({
      filters: this.filters,
      filterEnabled: this.filterEnabled,
      dateFilterFrom: this.dateFilterFrom,
      dateFilterTo: this.dateFilterTo
    });
  }

}
