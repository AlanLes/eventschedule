import * as moment from "moment";
import { Component, ViewChild } from '@angular/core';
import { List } from 'ionic-angular';
import { NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import { EventsByDay, EventDataModel, TripModel } from "../../shared/events-data.models";
import { UserDataService } from "../../shared/user-data.service";
import { EventsListPage } from "../events-list/events-list.page";
import { TripMapPage } from "../trip-map/trip-map.page";
import { prepareEventsByDays } from "../../shared/utilities";

@Component({
  selector: 'trip',
  templateUrl: 'trip.page.html'
})
export class TripPage {
  @ViewChild(List) list: List;

  public eventsToTrip: EventDataModel[];
  public eventsByDays: EventsByDay[];
  public isSaved = false;
  public trip: TripModel;

  constructor(private nav: NavController,
              private navParams: NavParams,
              private userDataService: UserDataService,
              private alertController: AlertController,
              private toastController: ToastController) {
  }

  ionViewDidLoad() {
    if (this.navParams.data['id']) {  //check if it's saved
      this.trip = this.navParams.data;
      this.filterPastEvents();
      this.isSaved = true;
      this.eventsToTrip = this.trip.eventsToTrip;
    } else {
      this.eventsToTrip = this.navParams.data.eventsToTrip;
    }
    this.eventsByDays = prepareEventsByDays(this.eventsToTrip);
  }

  public saveYourTrip() {
    let alert = this.alertController.create({
      title: 'Zapisywanie podróży',
      message: 'Wpisz nazwę pod którą chcesz zapamiętać tą podróż.',
      inputs: [
        {
          name: 'tripName',
          placeholder: 'np. sporty lekkoatletyczne 11-18.08'
        }
      ],
      buttons: [
        {
          text: 'Anuluj',
          role: 'cancel',
        },
        {
          text: 'Zapisz',
          handler: data => {
            //check if there's a tripName
            if (data.tripName.length) {
              this.userDataService.saveTripToDB(data.tripName, this.eventsToTrip)
                .then((trip) => {
                  let toast = this.toastController.create({
                    duration: 1500,
                    message: 'Z powodzeniem zapisano podróż do bazy danych.',
                    position: 'bottom'
                  });
                  toast.present();
                  this.isSaved = true;
                  this.trip = trip;
                })
                .catch(() => {
                  let toast = this.toastController.create({
                    duration: 1500,
                    message: 'Coś poszło nie tak, spróbuj ponownie.',
                    position: 'bottom'
                  });
                  toast.present();
                  this.isSaved = false;
                });
            }
          }
        }
      ]
    });
    alert.present();
  }

  public removeYourTrip(): void {
    let confirm = this.alertController.create({
      title: 'Usunąć podróż?',
      message: 'Czy na pewno chcesz usunąć ten element z listy zaplanowanych podróży?',
      buttons: [
        {
          text: 'Tak',
          handler: () => {
            this.userDataService.removeTripFromDB(this.trip.id).then(() => {
              this.isSaved = false;

              let tmpToast = this.toastController.create({
                duration: 1500,
                message: 'Z powodzeniem usunięto element z listy zapisanych podróży.',
                position: 'bottom'
              });
              tmpToast.present();
            });
          }
        },
        {
          text: 'Nie',
        }
      ]
    });
    confirm.present();
  }

  // widok z listą dni podróży - poszczególne elementy klikalne przekierowują do mapki z eventami i trasą po nich
  // na mapkach też zaznaczony dzień w którym jesteś i następny albo poprzedni z arraya + trasa  + button cofnięcia do listy
  public seeTripForThisDay(eventsByDay: EventsByDay): void {
    this.nav.push(TripMapPage, eventsByDay);
  }

  public seeEventsListForThisTrip(eventByDay: EventsByDay): void {
    this.list.closeSlidingItems();
    this.nav.push(EventsListPage, {events: eventByDay.events});
  }

  private filterPastEvents() {
    let filteredTrip = this.trip.eventsToTrip.filter((el) => {
      return moment(el.event.dateTime.dateTimeStart, 'YYYY-MM-DD HH:mm').isAfter(moment());
    });

    if (this.trip.eventsToTrip.length !== filteredTrip.length) {
      this.trip.eventsToTrip = filteredTrip;
      //remove and add again this trip into DataBase
      this.userDataService.removeTripFromDB(this.trip.id)
        .then(() => {
          this.userDataService.saveTripToDB(this.trip.tripName, this.trip.eventsToTrip)
            .then(() => {
              let tmpToast = this.toastController.create({
                duration: 3500,
                message: 'Niektóre z wydarzeń wchodzących w skład tej podróży już minęły. Zostały one usunięte z tej listy.',
                position: 'bottom'
              });
              tmpToast.present();
            });
        });
    }
  }

}
