import { Component } from '@angular/core';
import { AlertController, ToastController, LoadingController, NavController, NavParams } from 'ionic-angular';
import * as moment from "moment";

import { UserDataService } from "../../shared/user-data.service";
import { EventsApiService } from "../../shared/events-api.service";
import { EventDataModel } from "../../shared/events-data.models";
import { MapPage } from "../map/map.page";


@Component({
  selector: 'event-details',
  templateUrl: 'event-details.page.html',
})
export class EventDetailsPage {

  event: EventDataModel;

  isFollowed: boolean;
  dateStart: string;
  timeStart: string;
  dateEnd: string;
  timeEnd: string;


  constructor(private loadingController: LoadingController,
              private alertController: AlertController,
              private toastController: ToastController,
              private nav: NavController,
              private navParams: NavParams,
              private eventsApi: EventsApiService,
              private userDataService: UserDataService) {
    this.event = new EventDataModel();
  }

  ionViewDidLoad() {
    let selectedEventId = this.navParams.data;
    this.getEventData(selectedEventId);

    this.userDataService.isInDatabase(selectedEventId).then(res => this.isFollowed = res);
  }

  public toggleFollow(): void {
    //this event is already followed
    if (this.isFollowed) {
      let confirm = this.alertController.create({
        title: 'Usunąć z obserwowanych?',
        message: 'Czy na pewno chcesz usunąć ten element z listy obserwowanych wydarzeń?',
        buttons: [
          {
            text: 'Tak',
            handler: () => {
              this.userDataService.removeEventFromFollowed(this.event.event.id).then(() => {
                this.isFollowed = false;

                let tmpToast = this.toastController.create({
                  duration: 1500,
                  message: 'Z powodzeniem usunięto element z listy obserwowanych wydarzeń.',
                  position: 'bottom'
                });
                tmpToast.present();
              });
            }
          },
          {
            text: 'Nie',
          }
        ]
      });
      confirm.present();
    } else {  //this event isn't followed yet
      this.userDataService.addEventToFollowed(this.event).then(() => {
        this.isFollowed = true;

        let tmpToast = this.toastController.create({
          duration: 1500,
          message: 'Dodano element do listy obserwowanych wydarzeń.',
          position: 'bottom'
        });
        tmpToast.present();
      });
    }
  }


  public showMap(): void {
    this.nav.push(MapPage, this.event);
  }

  public goHome(): void {
    this.nav.popToRoot();
  }

  private getEventData(id: string): void {
    let loader = this.loadingController.create({
      content: 'Wczytuję informacje o wydarzeniu...',
      spinner: 'bubbles',
      // dismissOnPageChange: true    //it's broken in the present ionic version
    });

    loader.present().then(() => {
      this.eventsApi.getEventById(id)
        .subscribe(res => {
          this.event = res;
          this.formatDate(this.event);
          loader.dismiss();
        })
    })

  }

  private formatDate(event: EventDataModel): void {
    this.dateStart = moment(event.event.dateTime.dateTimeStart).format("DD.MM.YYYY");
    this.timeStart = moment(event.event.dateTime.dateTimeStart).format("HH:mm");
    this.dateEnd = moment(event.event.dateTime.dateTimeEnd).format("DD.MM.YYYY");
    this.timeEnd = moment(event.event.dateTime.dateTimeEnd).format("HH:mm");
  }
}
