import { Component, ViewChild } from '@angular/core';
import { Events, List } from 'ionic-angular';
import { AlertController, LoadingController, NavController, ToastController } from 'ionic-angular';
import { TripModel } from "../../shared/events-data.models";
import { UserDataService } from "../../shared/user-data.service";
import { TripPage } from "../trip/trip.page";
import * as _ from 'lodash';

@Component({
  selector: 'trip-list',
  templateUrl: 'trip-list.page.html',
})
export class TripListPage {
  @ViewChild(List) list: List;

  public trips: TripModel[];

  constructor(private nav: NavController,
              private loadingController: LoadingController,
              private events: Events,
              private alertController: AlertController,
              private toastController: ToastController,
              private userDataService: UserDataService) {
    this.trips = [];
  }

  ionViewDidLoad() {
    this.events.subscribe('savedTrips:refresh', () => this.refreshSavedTrips());
    let loader = this.loadingController.create({
      content: 'Wczytuję Twoje podróże...',
      spinner: 'bubbles',
      // dismissOnPageChange: true    //it's broken in the present ionic version
    });

    loader.present().then(() => {
      this.userDataService.getAllSavedTrips().then(trips => {
        this.trips = trips;
        loader.dismiss();
      });
    })
  }

  public goToTrip(trip: TripModel): void {
    this.nav.push(TripPage, trip);
  }

  public goToMain(): void {
    this.nav.popToRoot();
  }

  public removeTrip(trip: TripModel): void {
    let confirm = this.alertController.create({
      title: 'Usunąć podróż?',
      message: 'Czy na pewno chcesz usunąć ten element z listy zaplanowanych podróży?',
      buttons: [
        {
          text: 'Tak',
          handler: () => {
            this.userDataService.removeTripFromDB(trip.id).then(() => {
              _.remove(this.trips, (el) => el.id === trip.id);
              let toast = this.toastController.create({
                duration: 1500,
                message: 'Z powodzeniem usunięto element z listy zaplanowanych podróży.',
                position: 'bottom'
              });
              toast.present();
            });
          }
        },
        {
          text: 'Nie',
          handler: () => {
            this.list.closeSlidingItems();
          }
        }
      ]
    });
    confirm.present();
  }

  private refreshSavedTrips(): void {
    this.userDataService.getAllSavedTrips().then(res => this.trips = res);
  }

}
