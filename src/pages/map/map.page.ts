import { Component } from '@angular/core';
import { NavParams, LoadingController, AlertController } from 'ionic-angular';
import { UserDataService } from "../../shared/user-data.service";
import { GoogleLocationModel } from "../../shared/events-data.models";


@Component({
  templateUrl: 'map.page.html',
})
export class MapPage {

  map: any = {};
  destination: GoogleLocationModel;
  currentPosition;

  constructor(private navParams: NavParams,
              private loadingController: LoadingController,
              private alertController: AlertController,
              private userDataService: UserDataService) {
  }

  ionViewDidLoad() {
    let event = this.navParams.data;
    console.log(event);

    this.map = {
      lat: event.location.latitude,
      lng: event.location.longitude,
      markerLabel: event.location.name,
      zoom: 14
    }
  }

  public showWay() {
    let loader = this.loadingController.create({
      content: 'Pobieram informacje o Twoim aktualnym położeniu...',
      spinner: 'bubbles',
      // dismissOnPageChange: true    //it's broken in the present ionic version
    });
    loader.present().then(() => {
      this.userDataService.getPosition()
        .then(res => {
          console.log(res);
          this.currentPosition = res;
          this.destination = {
            lat: this.map.lat,
            lng: this.map.lng
          };
          loader.dismiss();
        })
        .catch(err => {
          loader.dismiss();
          let alert = this.alertController.create({
            title: 'Błąd',
            subTitle: 'Wystąpił błąd podczas pobierania informacji o Twoim aktualnym położeniu. Spróbuj ponownie później.',
            buttons: ['OK']
          });
          alert.present();
        });
    })
  }

}
