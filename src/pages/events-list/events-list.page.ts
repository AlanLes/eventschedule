import { Component } from '@angular/core';
import { LoadingController, NavController, NavParams, PopoverController } from 'ionic-angular';
import { EventsApiService } from "../../shared/events-api.service";
import { UserDataService } from "../../shared/user-data.service";
import { EventDataModel, FilterModel, PreparedFilterModel } from "../../shared/events-data.models";
import { EventDetailsPage } from "../event-details/event-details.page";
import { SettingsPopoverPage } from "../settings-popover/settings-popover.page";
import * as _ from 'lodash';
import * as moment from "moment";

@Component({
  selector: 'events-list',
  templateUrl: 'events-list.page.html',
})
export class EventsListPage {

  originEvents: EventDataModel[];
  events: EventDataModel[];

  sortByDate: string = '';
  sortByCity: string = '';
  orderPhrase: string = '';

  filterEnabled: boolean = false;
  initialFilters: PreparedFilterModel[];
  filters: PreparedFilterModel[];
  dateFilterFrom: string;
  dateFilterTo: string;
  noFilters = false;

  constructor(private loadingController: LoadingController,
              private nav: NavController,
              private navParams: NavParams,
              private eventsApi: EventsApiService,
              private userDataService: UserDataService,
              public popoverController: PopoverController) {

    this.dateFilterFrom = moment().format("YYYY-MM-DD");
    this.dateFilterTo = moment().format("YYYY-MM-DD");

    this.events = [];
  }

  ionViewDidLoad() {
    if (!this.navParams.data['events']) {
      this.noFilters = false;
      let loader = this.loadingController.create({
        content: 'Wczytuję wydarzenia...',
        spinner: 'bubbles',
        // dismissOnPageChange: true    //it's broken in the present ionic version
      });

      loader.present().then(() => {
        this.eventsApi.getAvailableFilters().then(filters => {
          this.filters = this.initialFilters = this.prepareFilters(filters);

          this.eventsApi.getAllEvents().then(events => {
            this.originEvents = events;
            this.events = this.originEvents;
            this.markFollowedEvents();
            loader.dismiss();
          })
        })
      })
    } else {
      this.originEvents = this.navParams.data.events;
      this.events = this.navParams.data.events;
      this.noFilters = true;
    }

  }

  ionViewWillEnter() {
    if (this.originEvents && this.events.length && !this.navParams.data['events']) {
      this.markFollowedEvents().then(() => {
        if (this.filterEnabled) {
          this.events = this.filterEvents();
        }
      });
    }
  }

  public selectEvent($event, id: string): void {
    this.nav.push(EventDetailsPage, id);
  }

  public openSettingsPopover(ev): void {
    let popover = this.popoverController.create(SettingsPopoverPage, {
      filters: this.filters,
      filterEnabled: this.filterEnabled,
      dateFilterFrom: this.dateFilterFrom,
      dateFilterTo: this.dateFilterTo
    }, {enableBackdropDismiss: false});
    popover.present({ev});

    popover.onDidDismiss((popoverData) => {
      this.filters = popoverData.filters;
      this.filterEnabled = popoverData.filterEnabled;
      this.dateFilterFrom = popoverData.dateFilterFrom;
      this.dateFilterTo = popoverData.dateFilterTo;

      if (this.filterEnabled) {
        this.events = this.filterEvents();
      } else {
        this.events = this.originEvents;
      }
    })
  }

  public sortBy(prop: string): void {
    switch (prop) {
      case 'date': {
        this.sortByDate === 'asc' ? this.sortByDate = 'desc' : this.sortByDate = 'asc';
        this.sortByCity = '';
        this.orderPhrase = 'event.dateTime.dateTimeStart';
        break;
      }
      case 'city': {
        this.sortByCity === 'asc' ? this.sortByCity = 'desc' : this.sortByCity = 'asc';
        this.sortByDate = '';
        this.orderPhrase = 'event.details.city';
        break;
      }
    }
  }

  private markFollowedEvents(): Promise<void> {
    let followedEvents = [];
    let notFollowedEvents = [];

    return new Promise<void>(resolve => {
      this.userDataService.getAllFollowedEvents().then(res => {
        followedEvents = res.map(el => {
          el['followed'] = true;
          return el;
        });

        notFollowedEvents = _.xorBy(followedEvents, this.originEvents, 'event.id');
        notFollowedEvents = notFollowedEvents.map(el => {
          el.followed = false;
          return el;
        });

        this.originEvents = _.concat(followedEvents, notFollowedEvents);
        this.events = this.originEvents;

        resolve();
      });
    });
  }

  private prepareFilters(filters: FilterModel[]): PreparedFilterModel[] {
    return _.map(filters, filter => {
      return {
        name: filter.name,
        description: filter.description,
        values: _.map(filter.values, val => {
          return {name: val, checked: false};
        })
      }
    });
  }

  private filterEvents(): EventDataModel[] {
    //_.intersection() Creates an array of unique values that are included in all given arrays
    let result = this.originEvents;
    let cityArr = [];
    let sportTypeArr = [];
    let eventSizeArr = [];
    let dateArr = [];

    let cityFilters = _.filter(this.filters[0].values, {'checked': true});
    let sportTypeFilters = _.filter(this.filters[1].values, {'checked': true});
    let eventSizeFilters = _.filter(this.filters[2].values, {'checked': true});

    //cityFilters selected
    if (cityFilters.length) {
      _.forEach(cityFilters, (v, k) => {
        cityArr.push(_.filter(this.originEvents, {event: {details: {city: v.name}}}));
        cityArr = _.flattenDeep(cityArr);
      });
      result = _.intersectionBy(cityArr, result, 'event.id');
    }

    //sportTypeFilters selected
    if (sportTypeFilters.length) {
      _.forEach(sportTypeFilters, (v, k) => {
        sportTypeArr.push(_.filter(this.originEvents, {event: {details: {sportType: v.name}}}));
        sportTypeArr = _.flattenDeep(sportTypeArr);
      });
      result = _.intersectionBy(sportTypeArr, result, 'event.id');
    }

    //eventSizeFilters selected
    if (eventSizeFilters.length) {
      _.forEach(eventSizeFilters, (v, k) => {
        eventSizeArr.push(_.filter(this.originEvents, {event: {details: {eventSize: v.name}}}));
        eventSizeArr = _.flattenDeep(eventSizeArr);
      });
      result = _.intersectionBy(eventSizeArr, result, 'event.id');
    }

    if (this.dateFilterFrom && this.dateFilterTo) {
      dateArr = _.filter(this.originEvents, ev =>
        (moment(this.dateFilterFrom).isSameOrBefore(moment(ev.event.dateTime.dateTimeStart), 'day') &&
          moment(this.dateFilterTo).isSameOrAfter(moment(ev.event.dateTime.dateTimeEnd), 'day')));
      result = _.intersectionBy(dateArr, result, 'event.id');
    }


    return _.flattenDeep(result);
  }
}
